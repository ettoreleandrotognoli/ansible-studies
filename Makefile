KEY_TYPE?=rsa
KEY_SIZE?=4096

roles/users/files/system/authorized_keys: .keys/system.key .keys/system.key.pub
	cp .keys/system.key.pub $@

%.key:
	mkdir -p $(shell dirname $@)
	ssh-keygen -f $@ -t ${KEY_TYPE} -b ${KEY_SIZE} -q -N ""


clean:
	rm -rf .keys/
	rm -f roles/users/files/system/authorized_keys
